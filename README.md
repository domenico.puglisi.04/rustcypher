# Algoritmo Cifratura Chiave Simmetrica

### Domenico Puglisi, 5 INF. I - A.S. 2021/2022

**Nota: Per compilare il codice è consigliato l'uso di un sistema Linux.
La compilazione richiede l'utilizzo del canale nightly di Rust.
Una build di release per Linux è inclusa.**

## Presentazione dell'Algoritmo e del Programma

L'algoritmo è un block cypher con grandezza del blocco e della chiave fisse a 256b, basato principalmente su sostituzione.
Il processo di cifratura è iterativo, con un valore predefinito di 16 iterazioni;
tuttavia, tale numero non è fisso e, se ritenuto opportuno, può essere selezionato a piacimento.

È prevista la possibilità di generare in maniera casuale una chiave; in alternativa, l'utente può fornire in input una propria chiave in formato esadecimale o, alternativamente, una stringa arbitraria di cui calcolare l'hash SHA-256 come chiave.

Il linguaggio scelto per l'implementazione dell'algoritmo è Rust, sia per preferenza personale
dell'implementatore che per i suoi diversi vantaggi.
Difatti, Rust è un linguaggio di programmazione di basso livello in grado di offrire
ottime garanzie di stabilità, sicurezza e correttezza del codice grazie al proprio
*Borrow Checker* senza però comprometterne l'efficienza computazionale.
In alternativa, sarebbe stato possibile utilizzare i linguaggi C o C++.

---

## Struttura del codice sorgente

Il codice è suddiviso in due file: il primo, src/lib.rs, contiene tutte le strutture dati
e le funzioni necessarie per l'implementazione dell'algoritmo; il secondo, src/main.rs,
è composto invece dal codice che permette all'utente finale di usufruire della libreria.

Per gestire il parsing dei parametri a riga di comando, la generazione di valori numerici casuali ed il calcolo delle hash SHA-256, in quanto oltre l'ambito di questa esercitazione, sono state utilizzate quattro librerie esterne, presenti nel file Cargo.toml ed elencate a seguire:

- [rand](https://crates.io/crates/rand): crate per la generazione di valori numerici casuali;
- [sha256](https://crates.io/crates/sha256): crate per il calcolo di hash SHA-256 su stringhe o collezioni di byte;
- [clap](https://crates.io/crates/clap): crate estremamente potente che permette di generare mediante l'uso di macro
  un parser per parametri a riga di comando in stile dichiarativo;
- [primitive-types](https://crates.io/crates/primitive-types): crate che implementa diversi tipi per i valori interi oltre i
  128 bit ufficialmente supportati da Rust;

Nessuna delle librerie è stata utilizzata per realizzare la sezione centrale di codice contenuta in src/lib.rs.
Sono state incluse per permettere all'implementatore di concentrarsi esclusivamente sull'algoritmo di cifratura.

## Struttura dell'algoritmo

Come già anticipato, l'algoritmo è un block cypher in cui sia la grandezza dei blocchi che quella della chiave sono fisse a 256b. Prima dell'esecuzione dell'algoritmo, il plaintext o corrispondente cyphertext viene suddiviso in gruppi di 256b, poi convertiti in blocchi. Ciascun blocco è a sua volta composto da quattro sotto-blocchi di 64b ciascuno; ogni iterazione, nello specifico, opera sui sotto-blocchi.
La suddivisione in sotto-blocchi riguarda anche la chiave,
composta quindi da quattro gruppi di 64 bit ciascuno.

La gestione di eventuale testo in chiaro rimanente di grandezza inferiore a quella di un blocco è gestita aggiungendo dei byte di padding dal valore pari al numero di byte di padding stesso. Così facendo, è possibile rimuovere facilmente i byte di padding al termine del processo di decifratura, in quanto se ne conosce il numero esatto.
I file di grandezza arbitraria non comportano dunque problema alcuno.

La schedulazione della chiave consiste nel ruotare ad ogni iterazione i bit dei singoli sottoblocchi che la compongono secondo dei valori presenti in una tabella nota.
Tali valori sono i seguenti: [3, 5, 7, 11], tutti numeri primi.
Così facendo, si generano dei valori di chiave differenti ad ogni iterazione effettuata, ottenendo migliori proprietà di diffusione.
Usando dei numeri dispari, inoltre, si riduce immensamente la probabilità che i bit delle sottochiavi utilizzate siano mai gli stessi.

Per quanto riguarda il processo iterativo, invece, i passaggi svolti dall'algoritmo sono i seguenti:

- Il primo e l'ultimo sottoblocco del plaintext sono combinati con i loro corrispettivi della chiave mediante XOR;
- I restanti due sottoblocchi sono combinati invece con i corrispondenti sottoblocchi della chiave
  con una somma modulo 2^64;
- Viene verificato un predicato; se la somma dei bit posti a uno del secondo e del terzo sottoblocco della chiave
  è un valore pari, allora sono invertiti i bit del secondo e del terzo sottoblocco di plaintext; altrimenti,
  sono invertiti i bit degli altri due sottoblocchi;
- Per ottenere una maggiore proprietà di diffusione, i due sottoblocchi centrali vengono scambiati l'uno con l'altro;
- I bit della chiave vengono ruotati come descritto in precedenza;
- Termina quindi un'iterazione dell'algoritmo.

Il processo di decifratura è strutturato inversamente rispetto a quello di cifratura; gli accorgimenti più importanti sono i seguenti:

- Prima dell'inizio del processo iterativo, viene calcolata la chiave corrispondente a quella dell'ultima iterazione
  di cifratura con una rotazione di un numero di bit corrispondente;
- La rotazione di bit in decifratura avviene verso destra piuttosto che sinistra ad ogni iterazione;
- Al posto della somma, si calcola la differenza modulo 2^16 nel secondo passaggio iterativo;
- Al termine del processo iterativo, i bit di padding aggiunti all'ultimo blocco devono essere rimossi.

Infine, al termine di ciascun processo iterativo, i bit di cyphertext del blocco prodotto sono combinati con i bit di plaintext del successivo per evitare che a blocchi di plaintext uguali corrisponda cyphertext uguale.