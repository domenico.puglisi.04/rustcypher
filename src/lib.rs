#![feature(array_chunks)]
#![feature(slice_as_chunks)]

use std::num::Wrapping;

/// Size of a block to be used in the cypher. 32B is the size of four u64's.
pub const BLOCK_SIZE: usize = 32;
const U64_SIZE: usize = (u64::BITS / 8) as usize;
const SUB_BLOCK_COUNT: usize = BLOCK_SIZE / U64_SIZE;
const SHIFT_TABLE: [u32; SUB_BLOCK_COUNT] = [3, 5, 7, 11];

/// Function that takes in a u8 slice of plaintext and returns a Vec<u8> of cyphertext.
/// The final block is padded accordingly if it does not match the block size constant.
pub fn encrypt(
    plaintext: &[u8],
    key: &[u64; SUB_BLOCK_COUNT],
    rounds: usize,
    chain_mode: bool,
) -> Vec<u8> {
    let starting_key = Block::new(key[0], key[1], key[2], key[3]);
    let mut cyphertext: Vec<u8> = Vec::with_capacity(plaintext.len());

    let mut last_encrypted_block = Block::new(0, 0, 0, 0);
    for plaintext_block in Block::iter_from_bytes(plaintext, true) {
        let mut key = starting_key;

        // Initialize encrypted block either with the plaintext or by chaining with last output
        let mut encrypted_block = if chain_mode {
            Block {
                blk0: plaintext_block.blk0 ^ last_encrypted_block.blk0,
                blk1: plaintext_block.blk1 ^ last_encrypted_block.blk1,
                blk2: plaintext_block.blk2 ^ last_encrypted_block.blk2,
                blk3: plaintext_block.blk3 ^ last_encrypted_block.blk3,
            }
        } else {
            plaintext_block
        };

        for _round in 0..rounds {
            encrypted_block = encrypt_round(&encrypted_block, &mut key)
        }

        last_encrypted_block = encrypted_block;
        cyphertext.extend_from_slice(&encrypted_block.to_array());
    }

    cyphertext
}

/// Function that takes in a u8 slice of cyphertext and returns a Vec<u8> of plaintext.
/// The final block is checked for padding and trimmed before being returned.
pub fn decrypt(
    cyphertext: &[u8],
    key: &[u64; SUB_BLOCK_COUNT],
    rounds: usize,
    chain_mode: bool,
) -> Vec<u8> {
    let starting_key = compute_round_key(&Block::new(key[0], key[1], key[2], key[3]), rounds);
    let mut plaintext: Vec<u8> = Vec::new();

    let mut last_encrypted_block = Block::new(0, 0, 0, 0);
    for cyphertext_block in Block::iter_from_bytes(cyphertext, false) {
        let mut key = starting_key;
        let mut decrypted_block = cyphertext_block;

        for _round in (0..rounds).rev() {
            decrypted_block = decrypt_round(&decrypted_block, &mut key);
        }

        // Undo block chaining if chain mode is enabled
        if chain_mode {
            decrypted_block = Block {
                blk0: decrypted_block.blk0 ^ last_encrypted_block.blk0,
                blk1: decrypted_block.blk1 ^ last_encrypted_block.blk1,
                blk2: decrypted_block.blk2 ^ last_encrypted_block.blk2,
                blk3: decrypted_block.blk3 ^ last_encrypted_block.blk3,
            };
        }

        last_encrypted_block = cyphertext_block;

        plaintext.extend_from_slice(&decrypted_block.to_array());
    }

    // Ensure to trim padding bytes at the end
    if let Some(padded_amount) = plaintext.last().cloned() {
        plaintext.resize(plaintext.len() - padded_amount as usize, 0);
    }

    plaintext
}

/// Simple Block struct comprised of four u64's (making up a 256b unsigned value)
#[derive(Default, Copy, Clone, PartialEq, Debug)]
struct Block {
    blk0: u64,
    blk1: u64,
    blk2: u64,
    blk3: u64,
}

impl Block {
    const fn new(blk0: u64, blk1: u64, blk2: u64, blk3: u64) -> Block {
        Block {
            blk0,
            blk1,
            blk2,
            blk3,
        }
    }

    /// Function that takes in a reference to an array of 32 bytes and returns the
    /// corresponding block.
    fn from_slice(bytes: &[u8; BLOCK_SIZE]) -> Block {
        let sub_blocks: Vec<u64> = {
            // split the u8's into groups of 8 bytes, equal to the size of a u64
            let (u64_groups, _remainder): (&[[u8; U64_SIZE]], &[u8]) =
                bytes.as_chunks::<U64_SIZE>();

            assert_eq!(_remainder.len(), 0);

            // Convert each group of u8's into a single u64 value
            u64_groups
                .iter()
                .map(|bytes: &[u8; U64_SIZE]| u64::from_ne_bytes(*bytes))
                .collect::<Vec<u64>>()
        };

        assert_eq!(sub_blocks.len(), SUB_BLOCK_COUNT);

        // Construct a block using the four u64 values
        Block {
            blk0: sub_blocks[0],
            blk1: sub_blocks[1],
            blk2: sub_blocks[2],
            blk3: sub_blocks[3],
        }
    }

    /// Function that takes in a u8 slice asserted to be less than the size of a Block
    /// and returns a padded Block .
    fn from_slice_with_padding(bytes: &[u8]) -> Block {
        let remainder_bytes = BLOCK_SIZE - bytes.len();
        assert!(
            remainder_bytes > 0 && remainder_bytes <= BLOCK_SIZE,
            "Remainder is of erroneous size = {}",
            remainder_bytes
        );

        let padded_block = {
            let mut block_to_pad = [remainder_bytes as u8; BLOCK_SIZE];

            // Overwrite the starting range with bytes from the original slice,
            // then evaluates the padded block
            block_to_pad[..BLOCK_SIZE - remainder_bytes].clone_from_slice(bytes);
            block_to_pad
        };

        Block::from_slice(&padded_block)
    }

    /// Method that returns the corresponding u8 array to the current block.
    fn to_array(self) -> [u8; BLOCK_SIZE] {
        let array = [self.blk0, self.blk1, self.blk2, self.blk3];
        array
            .iter()
            .flat_map(|u64_bytes| u64_bytes.to_ne_bytes())
            .collect::<Vec<u8>>()
            .try_into()
            .expect("Output array does not match intended size")
    }

    /// Function that takes in a u8 slice and returns an iterator over its contents
    /// converted to Blocks, padding the remainder if specified as a parameter.
    fn iter_from_bytes(
        bytes: &[u8],
        mut check_remainder: bool,
    ) -> Box<dyn Iterator<Item = Block> + '_> {
        let mut iter = bytes.array_chunks::<BLOCK_SIZE>();
        Box::new(std::iter::from_fn(move || {
            if let Some(next) = iter.next() {
                Some(Block::from_slice(next))
            } else if check_remainder {
                check_remainder = false;
                Some(Block::from_slice_with_padding(iter.remainder()))
            } else {
                None
            }
        }))
    }
}

/// Function that performs a single round of encryption on the given plaintext Block.
fn encrypt_round(plaintext: &Block, key: &mut Block) -> Block {
    // Perform the initial xor's on each sub-block
    let mut encrypted_block = Block {
        blk0: plaintext.blk0 ^ key.blk0,
        blk1: (Wrapping(plaintext.blk1) + Wrapping(key.blk1)).0,
        blk2: (Wrapping(plaintext.blk2) + Wrapping(key.blk2)).0,
        blk3: plaintext.blk3 ^ key.blk3,
    };

    // Verify the predicate and flip the bits of given blocks accordingly
    if (key.blk1.count_ones() + key.blk2.count_ones()) % 2 == 0 {
        encrypted_block.blk1 ^= u64::MAX;
        encrypted_block.blk2 ^= u64::MAX;
    } else {
        encrypted_block.blk0 ^= u64::MAX;
        encrypted_block.blk3 ^= u64::MAX;
    }

    // Swap the two central blocks to achieve better diffusion properties
    use std::mem;
    mem::swap(&mut encrypted_block.blk1, &mut encrypted_block.blk2);

    // Rotate the key sub-blocks left according to the shift-table
    key.blk0 = key.blk0.rotate_left(SHIFT_TABLE[0]);
    key.blk1 = key.blk1.rotate_left(SHIFT_TABLE[1]);
    key.blk2 = key.blk2.rotate_left(SHIFT_TABLE[2]);
    key.blk3 = key.blk3.rotate_left(SHIFT_TABLE[3]);

    // Return the encrypted block
    encrypted_block
}

/// Function that computes the initial key state of any given round.
fn compute_round_key(key: &Block, rounds: usize) -> Block {
    Block {
        blk0: key
            .blk0
            .rotate_left(SHIFT_TABLE[0] * rounds as u32 % u64::BITS),
        blk1: key
            .blk1
            .rotate_left(SHIFT_TABLE[1] * rounds as u32 % u64::BITS),
        blk2: key
            .blk2
            .rotate_left(SHIFT_TABLE[2] * rounds as u32 % u64::BITS),
        blk3: key
            .blk3
            .rotate_left(SHIFT_TABLE[3] * rounds as u32 % u64::BITS),
    }
}

/// Function that performs a round of decryption on the given cyphertext Block.
fn decrypt_round(cyphertext: &Block, key: &mut Block) -> Block {
    // Swap the two central sub-blocks back
    let mut decrypted_block = Block {
        blk1: cyphertext.blk2,
        blk2: cyphertext.blk1,
        ..*cyphertext
    };

    // Rotate the keys to the right according to the same shift table
    key.blk0 = key.blk0.rotate_right(SHIFT_TABLE[0]);
    key.blk1 = key.blk1.rotate_right(SHIFT_TABLE[1]);
    key.blk2 = key.blk2.rotate_right(SHIFT_TABLE[2]);
    key.blk3 = key.blk3.rotate_right(SHIFT_TABLE[3]);

    // Verify the given predicate and flip back the bits of the decrypted block accordingly
    if (key.blk1.count_ones() + key.blk2.count_ones()) % 2 == 0 {
        decrypted_block.blk1 ^= u64::MAX;
        decrypted_block.blk2 ^= u64::MAX;
    } else {
        decrypted_block.blk0 ^= u64::MAX;
        decrypted_block.blk3 ^= u64::MAX;
    }

    // Apply the final Xor's with the key to restore the original block values
    decrypted_block = Block {
        blk0: decrypted_block.blk0 ^ key.blk0,
        blk1: (Wrapping(decrypted_block.blk1) - Wrapping(key.blk1)).0,
        blk2: (Wrapping(decrypted_block.blk2) - Wrapping(key.blk2)).0,
        blk3: decrypted_block.blk3 ^ key.blk3,
    };

    // Return the decrypted block
    decrypted_block
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Default amount of cypher rounds to be used when testing
    const CYPHER_ROUNDS: usize = 16;

    /// Test that asserts u8 slice to Block conversion logic
    #[test]
    fn slice_to_block() {
        let bytes: Vec<u8> = [0xAA_u64, 0xBB_u64, 0xCC_u64, 0xDD_u64]
            .into_iter()
            .flat_map(|num| num.to_ne_bytes())
            .collect();

        for byte_group in bytes.array_chunks::<BLOCK_SIZE>() {
            let block = Block::from_slice(byte_group);
            assert_eq!(
                block,
                Block {
                    blk0: 0xAA,
                    blk1: 0xBB,
                    blk2: 0xCC,
                    blk3: 0xDD
                }
            );
        }
    }

    /// Test that asserts Block to u64 array conversion
    #[test]
    fn block_to_array() {
        let array: Vec<u8> = [0xAA_u64, 0xBB_u64, 0xCC_u64, 0xDD_u64]
            .into_iter()
            .flat_map(|num| num.to_ne_bytes())
            .collect();

        assert_eq!(array.len(), BLOCK_SIZE);

        let block = Block::new(0xAA_u64, 0xBB_u64, 0xCC_u64, 0xDD_u64);
        assert_eq!(array, block.to_array());
    }

    /// Test that asserts encryption and decryption of a series of bytes
    #[test]
    fn encrypt_and_decrypt_bytes() {
        let chain_mode = true;
        let test_key: [u64; SUB_BLOCK_COUNT] = [
            0x2cf24dba5fb0a30e,
            0x26e83b2ac5b9e29e,
            0x1b161e5c1fa7425e,
            0x73043362938b9824,
        ];

        let plaintext = Block::new(0xAA, 0xBB, 0xCC, 0xDD).to_array().to_vec();
        let cyphertext = encrypt(&plaintext, &test_key, CYPHER_ROUNDS, chain_mode);
        let computed_plaintext = decrypt(&cyphertext, &test_key, CYPHER_ROUNDS, chain_mode);

        assert_eq!(plaintext, computed_plaintext);
    }

    /// Test that asserts encryption and decryption of a UTF-8 string
    #[test]
    fn encrypt_and_decrypt_text() {
        let chain_mode = true;
        let message = String::from("Hello World, this is a very long string");
        let test_key: [u64; SUB_BLOCK_COUNT] = [
            0x2cf24dba5fb0a30e,
            0x26e83b2ac5b9e29e,
            0x1b161e5c1fa7425e,
            0x73043362938b9824,
        ];

        let plaintext = message.as_bytes();
        let cyphertext = encrypt(plaintext, &test_key, CYPHER_ROUNDS, chain_mode);
        let computed_plaintext =
            String::from_utf8_lossy(&decrypt(&cyphertext, &test_key, CYPHER_ROUNDS, chain_mode))
                .to_string();

        assert_eq!(message, computed_plaintext);
    }
}
