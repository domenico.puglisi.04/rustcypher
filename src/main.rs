//! Simple CLI program to expose the encryption and decryption
//! functionality to end users.

use std::io::{stdin, stdout, BufRead, BufReader, Write};
use std::path::PathBuf;

use clap::{ArgEnum, Parser};
use primitive_types::U256;
use rand::{thread_rng, Rng};

use cypher_school::{decrypt, encrypt};

/// Command-line program which implements a simple encryption algorithm
#[derive(Parser, Debug)]
#[clap(author = "Domenico Puglisi", version, about, long_about = None)]
struct ProgramArgs {
    /// String containing the key to be used
    #[clap(short, long)]
    key: Option<String>,

    /// Flag to signal whether the key is in hexadecimal format
    #[clap(short = 'x', long = "--hex")]
    use_hex_format: bool,

    /// Flag to enable CBC encryption/decryption mode
    #[clap(short = 'c', long = "--cbc")]
    use_cbc_mode: bool,

    /// Number of encryption/decryption rounds to perform
    #[clap(short = 'r', long, default_value_t = 16)]
    cypher_rounds: usize,

    /// File to encrypt the contents of
    #[clap(short = 'i', long, parse(from_os_str))]
    input_file: Option<PathBuf>,

    /// File to write encrypted output to
    #[clap(short = 'o', long, parse(from_os_str))]
    output_file: Option<PathBuf>,

    /// Operation to perform
    #[clap(arg_enum)]
    operation: Operation,
}

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq, Eq, Ord, ArgEnum)]
enum Operation {
    Encrypt,
    Decrypt,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = ProgramArgs::parse();

    let key = match args.key {
        Some(arbitrary_string) if !args.use_hex_format => {
            U256::from_str_radix(&sha256::digest(arbitrary_string), 16).unwrap()
        }
        Some(key_hex_string) => U256::from_str_radix(&key_hex_string, 16)?,
        None => {
            let key = produce_random_key();
            // TODO: Abstract this based on system byte order
            // Reverse the bytes due to system endianness
            eprintln!("{:x}{:x}{:x}{:x}", key.0[3], key.0[2], key.0[1], key.0[0]);
            key
        }
    };

    let input_data = {
        if let Some(input_path) = args.input_file {
            std::fs::read(input_path)?
        } else {
            let mut buffer: Vec<u8> = Vec::new();
            let mut stdin = BufReader::new(stdin());
            stdin.read_until(b'\x00', &mut buffer)?;
            buffer
        }
    };

    let output_data: Vec<u8> = match args.operation {
        Operation::Encrypt => encrypt(
            input_data.as_slice(),
            &key.0,
            args.cypher_rounds,
            args.use_cbc_mode,
        ),
        Operation::Decrypt => decrypt(
            input_data.as_slice(),
            &key.0,
            args.cypher_rounds,
            args.use_cbc_mode,
        ),
    };

    if let Some(output_path) = args.output_file {
        std::fs::write(output_path, output_data.as_slice())?;
    } else {
        stdout().write_all(output_data.as_slice())?;
    }

    Ok(())
}

fn produce_random_key() -> U256 {
    let mut array = [0u64; 4];
    thread_rng().fill(&mut array);

    U256(array)
}
